<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri = "http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="container-fluid">
	<sf:form method="get" modelAttribute="filtroForm" cssClass="form-inline">
		<div class="form-group mt-2">
			<sf:label for="desde" path="desde" cssClass="ml-1 mr-3">Desde</sf:label>
			<sf:input path="desde" type="date" cssClass="form-control form-control-sm mr-2" id="desde" autofocus="true" />
			
			<sf:label for="hasta" path="hasta" cssClass="ml-1 mr-3">Hasta</sf:label>
			<sf:input path="hasta" type="date" cssClass="form-control form-control-sm mr-2" id="hasta"/>
			<br/>
			<button type="submit" class="btn btn-primary ml-1 mr-2">Filtrar</button>
			<a href="${contextPath}/cgm/homepage" class="btn btn-secondary" role="button">Volver</a>
		</div>
	</sf:form>
	<br/>
	<table class="table table-sm table-striped">
		<thead>
			<tr>
				<th scope="col" style="width: 10%">Desde</th>
				<th scope="col" style="width: 10%">Hasta</th>
				<th scope="col">Peticion</th>
				<th scope="col">Tarea</th>
				<th scope="col" style="width: 5%">Horas</th>
				<th scope="col">Comentarios</th>
			</tr>
		</thead>
			
		<tbody>
			<c:forEach items="${horas}" var="h">
				<tr>
					<fmt:formatDate value="${h.desde}" var="f_desde" pattern="dd/MM/yyyy" />
					<fmt:formatDate value="${h.hasta}" var="f_hasta" pattern="dd/MM/yyyy" />
				
				  	<td>${f_desde}</td>
				  	<td>${f_hasta}</td>
				  	<td>${h.peticion.interno}</td>
				  	<td>${h.tarea.codigo}</td>
				  	<td class="text-right">${h.horas}:<fmt:formatNumber pattern="00" value="${h.minutos}"/></td>
				  	<td>${fn:substring(h.comentarios, 0, 40)}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>