<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<h1><spring:message code="cgm.welcome"/></h1>
<a href="<c:url value="/horas/detalle"/>">Detalle de horas</a> |
<a href="<c:url value="/horas/cargar"/>">Cargar horas</a> |
<a href="<c:url value="/spitter/register"/>">Register</a>
