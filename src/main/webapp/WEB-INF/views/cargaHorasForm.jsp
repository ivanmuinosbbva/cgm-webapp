<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="container-fluid">
	<h3>Carga de horas</h3>
	
	<br/>
			
	<div class="row">
		<div class="col-sm">
			<sf:form method="post" modelAttribute="cargaHora" action="guardar">
				<sf:errors element="div" cssClass="errors" path="*"/>
				
				<div class="form-group">
					<sf:label for="desde" path="desde">Desde</sf:label>
					<sf:input path="desde" type="date" class="form-control" id="desde" autofocus="true" />
				</div>
				
				<div class="form-group">
					<sf:label path="hasta" for="hasta">Hasta</sf:label>
					<sf:input path="hasta" type="date" class="form-control" id="hasta" />
				</div>
				
				<select class="custom-select">
				  	<option selected>Cargar horas a...</option>
				  	<option value="1">Petici�n</option>
				  	<option value="2">Tarea</option>
				</select>
				
				<hr/>
				
				<div class="form-group">
					<sf:label path="peticion" for="seleccionarPeticion">Petici�n</sf:label>
					<sf:select path="peticion" cssClass="form-control" id="seleccionarPeticion">
						<option value="60171" label="60171: Alta de nuevos listados en Sia"/>
						<option value="62741" label="62741: Ordenamiento visual en los estilos predefinidos"/>
						<option value="59336" label="59336: Migraci�n de la web de Becas a Spring"/>
						<option value="63255" label="63255: Migrar CGM a Spring framework"/>
						<option value="61707" label="61707: Reemplazo de aplicaci�n GPA por Profiling"/>
					</sf:select>
				</div>
				
				<div class="form-group">
					<sf:label path="tipo" for="clasificacion">Clasificaci�n</sf:label>
					<sf:select path="tipo" class="form-control" id="clasificacion">
						<sf:options items="${tipos}" itemValue="codigo" itemLabel="descripcion"/>
					</sf:select>
				</div>
				
				<div class="form-group">
					<sf:label path="horas" for="horas">Horas</sf:label>
					<sf:input path="horas" type="number" class="form-control" name="horas" min="0"/>
					
					<sf:label path="minutos" for="minutos">Minutos</sf:label>
					<sf:input path="minutos" type="number" class="form-control" name="minutos" min="0" max="59"/>
				</div>
				
				<div class="form-group">
					<sf:label path="comentarios" for="detalle">Detalle adicional</sf:label>
					<sf:textarea path="comentarios" class="form-control" id="detalle" rows="3"></sf:textarea>
				</div>
				
				<div class="form-check">
					<input type="checkbox" class="form-check-input" id="recordar">
					<label class="form-check-label" for="recordar">Recordarme</label>
				</div>
				
				<br/>
				
				<button type="submit" class="btn btn-primary">Guardar</button>
				<a href="${contextPath}/cgm/horas/detalle" class="btn btn-secondary" role="button">Volver</a>
			</sf:form>
		</div>
		
		<div class="col-sm">
			<table class="table table-sm table-striped">
				<thead>
					<tr>
						<th scope="col">D�a</th>
						<th scope="col">Cargadas</th>
						<th scope="col">Pendientes</th>
					</tr>
				</thead>
				
				<tbody>
					<c:forEach var="entry" items="${feriados}">
						<c:choose>
							<c:when test="${entry.value == true}">
								<tr class="table-dark">
								  <td>${entry.key}</td>
								  <td>-</td>
								  <td>-</td>
								</tr>
							</c:when>
							<c:otherwise>
								<tr>
								  <td>${entry.key}</td>
								  <td>8</td>
								  <td>0</td>
								</tr>
							</c:otherwise>
						</c:choose>
					</c:forEach>
					<tr class="table-warning">
					  <td>05/05/2018</td>
					  <td>5</td>
					  <td>3</td>
					</tr>
				</tbody>
				
			</table>
		</div>
	</div>
</div>