package ar.com.bbva.micro.cgm.model;

import java.util.Date;

public class HoraTrabajada {
	private Recurso recurso;
	private Date desde;
	private Date hasta;
	private String tipo;
	private long horas;
	private long minutos;
	//private long valor;
	private Peticion peticion;
	private Tarea tarea;
	private String comentarios;
	
	public HoraTrabajada() {}
	
	public HoraTrabajada(Recurso recurso, Date desde, Date hasta, String tipo, long horas, long minutos, Peticion peticion, Tarea tarea,
			String comentarios) {
		super();
		this.recurso = recurso;
		this.desde = desde;
		this.hasta = hasta;
		this.tipo = tipo;
		this.peticion = peticion;
		this.tarea = tarea;
		this.comentarios = comentarios;
	}
	
	public long getValor() {
		return minutos + (horas * 60);
	}

	public Recurso getRecurso() {
		return recurso;
	}

	public void setRecurso(Recurso recurso) {
		this.recurso = recurso;
	}
	
	public Date getDesde() {
		return desde;
	}
	public void setDesde(Date desde) {
		this.desde = desde;
	}
	public Date getHasta() {
		return hasta;
	}
	public void setHasta(Date hasta) {
		this.hasta = hasta;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public long getHoras() {
		//return valor / 60;
		return horas;
	}
	
	public long getMinutos() {
		return minutos;
		/*
		if (valor % 60 == 0) {
			return 0;
		} else {
			long horas = valor / 60;
			return valor - (horas * 60);
		}
		*/
	}

	public void setMinutos(long minutos) {
		this.minutos = minutos;
		//this.valor = minutos + (horas * 60);
	}
	
	public void setHoras(long horas) {
		this.horas = horas;
		//this.valor = minutos + (horas + 60);
	}

	public Peticion getPeticion() {
		return peticion;
	}
	public void setPeticion(Peticion peticion) {
		this.peticion = peticion;
	}
	public Tarea getTarea() {
		return tarea;
	}
	public void setTarea(Tarea tarea) {
		this.tarea = tarea;
	}
	public String getComentarios() {
		return comentarios;
	}
	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}
}
