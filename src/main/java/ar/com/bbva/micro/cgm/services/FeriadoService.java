package ar.com.bbva.micro.cgm.services;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import ar.com.bbva.micro.cgm.dao.FeriadoRepository;
import ar.com.bbva.micro.cgm.model.Feriado;

@Service
public class FeriadoService {
	Logger logger = LoggerFactory.getLogger(FeriadoService.class);
	
	@Autowired
	@Qualifier("jdbcFeriadoRepository")
	FeriadoRepository repository;
	
	public List<Feriado> findAll() {
		logger.debug("findAll()");
		return repository.findAll();
	}
	
	public List<Feriado> findAll(Date desde, Date hasta) {
		logger.debug("findAll(desde, hasta)");
		return repository.findAll(desde, hasta);
	}
	
	public Map<Feriado, Boolean> getPeriodo(Date desde, Date hasta) {
		Map<Feriado, Boolean> periodo = new TreeMap<>();
		List<Feriado> feriados = repository.findAll(desde, hasta);
		
		for (Date fecha = desde; fecha.compareTo(hasta) < 0; fecha = DateUtils.addDays(fecha, 1)) {
			Feriado feriado = new Feriado(fecha);
			
			if (feriados.contains(feriado)) {
				logger.debug(feriado + " es un feriado");
				periodo.put(feriado, Boolean.TRUE);
			} else {
				logger.debug(feriado + " es un día normal");
				periodo.put(feriado, Boolean.FALSE);
			}
		}
		return periodo;
	}
}
