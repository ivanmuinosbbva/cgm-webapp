package ar.com.bbva.micro.cgm.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.stereotype.Repository;

import ar.com.bbva.micro.cgm.dao.HorasRepository;
import ar.com.bbva.micro.cgm.dao.mappers.HoraTrabajadaMapper;
import ar.com.bbva.micro.cgm.model.HoraTrabajada;
import ar.com.bbva.micro.cgm.model.Tarea;

@Repository("jdbcHorasRepository")
public class JdbcHorasRepository implements HorasRepository {
	Logger logger = LoggerFactory.getLogger(JdbcHorasRepository.class);
	
	private static final String INSERT_HORAS = "insert into HorasTrabajadas " + 
			"(cod_recurso, cod_tarea, pet_nrointerno, fe_desde, fe_hasta, horas, trabsinasignar, observaciones, audit_user, audit_date) " + 
			"values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private JdbcOperations jdbcOperations;
	
	@Autowired
	public JdbcHorasRepository(JdbcOperations jdbcOperations) {
		this.jdbcOperations = jdbcOperations;
	}
	
	@Override
	public List<HoraTrabajada> findBy(String recurso, Date desde, Date hasta) {
		logger.debug("Viene por aca...");
		List<HoraTrabajada> horas = new ArrayList<HoraTrabajada>();
		Object[] args = {recurso, desde, hasta};
		
		horas = jdbcOperations.query(
				"SELECT cod_recurso, cod_tarea, pet_nrointerno, fe_desde, fe_hasta, horas, observaciones " + 
				"FROM HorasTrabajadas " + 
				"WHERE cod_recurso = ? and fe_desde >= ? and fe_hasta <= ? order by fe_desde",
				args,
				new HoraTrabajadaMapper());
		logger.debug("Tamanio: " + horas.size());
		return horas;
	}
	
	@Override
	public List<HoraTrabajada> findById(String recurso) {
		
		List<HoraTrabajada> horas = new ArrayList<HoraTrabajada>();
		horas = jdbcOperations.query("select cod_recurso, cod_tarea, pet_nrointerno, fe_desde, fe_hasta, horas, observaciones from HorasTrabajadas where cod_recurso = 'XA00309' order by fe_desde", new HoraTrabajadaMapper());
		
		return horas;
	}

	@Override
	public List<HoraTrabajada> findHoras(long max, int count) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HoraTrabajada findOne() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(HoraTrabajada hora) {
		/*
		SqlParameterSource beanParams = new BeanPropertySqlParameterSource(hora);
		
		String sqlQuery = "INSERT INTO HorasTrabajadas (cod_recurso, cod_tarea, pet_nrointerno, fe_desde, fe_hasta, horas, trabsinasignar, observaciones, audit_user, audit_date) " + 
				  "VALUES (:hora.getRecurso().getLegajo(), :yearOfIncorporation, :postalCode, :employeeCount, :slogan)";
		
		return operations.update(sqlQuery, beanParams) == 1;
		*/
		
		if (hora.getTarea() == null) {
			hora.setTarea(new Tarea(""));
		}

		jdbcOperations.update(INSERT_HORAS, 
				hora.getRecurso().getLegajo(),
				hora.getTarea().getCodigo(),
				hora.getPeticion().getInterno(),
				hora.getDesde(),
				hora.getHasta(),
				hora.getValor(),
				"N",
				hora.getComentarios(),
				"A125958",
				new Date());
	}
}
