package ar.com.bbva.micro.cgm.dao;

import java.util.List;

import ar.com.bbva.micro.cgm.model.Peticion;

public interface PeticionRepository {
	
	List<Peticion> findById(long interno);
	
	List<Peticion> findByActivas();
	
	List<Peticion> findByRecurso(String recurso);
}
