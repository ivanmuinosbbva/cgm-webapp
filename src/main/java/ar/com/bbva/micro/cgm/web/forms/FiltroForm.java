package ar.com.bbva.micro.cgm.web.forms;

public class FiltroForm {
	private String recurso;
	private String desde;
	private String hasta;
	
	public FiltroForm() {}
	
	public FiltroForm(String recurso, String desde, String hasta) {
		super();
		this.recurso = recurso;
		this.desde = desde;
		this.hasta = hasta;
	}

	public String getRecurso() {
		return recurso;
	}
	public void setRecurso(String recurso) {
		this.recurso = recurso;
	}
	public String getDesde() {
		return desde;
	}
	public void setDesde(String desde) {
		this.desde = desde;
	}
	public String getHasta() {
		return hasta;
	}
	public void setHasta(String hasta) {
		this.hasta = hasta;
	}
}