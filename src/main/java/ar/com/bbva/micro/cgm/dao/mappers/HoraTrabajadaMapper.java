package ar.com.bbva.micro.cgm.dao.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import ar.com.bbva.micro.cgm.model.HoraTrabajada;
import ar.com.bbva.micro.cgm.model.Peticion;
import ar.com.bbva.micro.cgm.model.Recurso;
import ar.com.bbva.micro.cgm.model.Tarea;

public class HoraTrabajadaMapper implements RowMapper<HoraTrabajada> {

	@Override
	public HoraTrabajada mapRow(ResultSet rs, int rowNum) throws SQLException {
		HoraTrabajada hora = new HoraTrabajada();
		long minutos;
		long horas;
		
		hora.setDesde(rs.getDate("fe_desde"));
		hora.setHasta(rs.getDate("fe_hasta"));
		hora.setRecurso(new Recurso(rs.getString("cod_recurso")));
		hora.setTarea(new Tarea(rs.getString("cod_tarea")));
		hora.setPeticion(new Peticion(rs.getLong("pet_nrointerno")));
		//hora.setTipo(rs.getString("tipo"));
		//hora.setValor(rs.getLong("horas"));
		
		if (rs.getLong("horas") % 60 == 0) {
			minutos = 0;
		} else {
			long h = rs.getLong("horas") / 60;
			minutos = rs.getLong("horas") - (h * 60);
		}
		horas = rs.getLong("horas") / 60;
		
		hora.setHoras(horas);
		hora.setMinutos(minutos);
		hora.setTipo("DESA");
		hora.setComentarios(rs.getString("observaciones"));
		return hora;
	}
}
