package ar.com.bbva.micro.cgm.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class Feriado implements Comparable<Feriado>{
	private Date fecha;
	
	public Feriado() {}
	
	public Feriado(Date fecha) {
		this.fecha = fecha;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	@Override
	public String toString() {
		return new SimpleDateFormat("dd-MM-yyyy").format(fecha);
	}

	@Override
	public boolean equals(Object that) {
		return EqualsBuilder.reflectionEquals(this, that, "fecha");
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this, "fecha");
	}
	
	@Override
	public int compareTo(Feriado other) {
	    return this.fecha.compareTo(other.fecha);
	}
}
