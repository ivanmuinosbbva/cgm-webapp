package ar.com.bbva.micro.cgm.web;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ar.com.bbva.micro.cgm.enums.TipoHora;
import ar.com.bbva.micro.cgm.model.Feriado;
import ar.com.bbva.micro.cgm.model.HoraTrabajada;
import ar.com.bbva.micro.cgm.model.Recurso;
import ar.com.bbva.micro.cgm.services.FeriadoService;
import ar.com.bbva.micro.cgm.services.HorasService;
import ar.com.bbva.micro.cgm.web.forms.FiltroForm;

@Controller
@RequestMapping("/horas")
public class HorasController {
	Logger logger = LoggerFactory.getLogger(HorasController.class);
	
	@InitBinder
	private void dateBinder(WebDataBinder binder) {
	    //The date format to parse or output your dates
	    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	    //Create a new CustomDateEditor
	    CustomDateEditor editor = new CustomDateEditor(dateFormat, true);
	    //Register it as custom editor for the Date type
	    binder.registerCustomEditor(Date.class, editor);
	}
	
	@Autowired
	@Qualifier("horasService")
	HorasService horasService;
	
	@Autowired
	@Qualifier("feriadoService")
	FeriadoService feriadoService;
	
	@RequestMapping(value="/detalle", method=RequestMethod.GET)
	public String detalleHoras(FiltroForm form, Model model) throws Exception {
		logger.debug("detalleHoras()");

		
		logger.debug("Viene desde: " + form.getDesde());
		
		String recurso = form.getRecurso();
		
		//DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		
		//DateTime dateTime = FORMATTER.parseDateTime("2005-nov-12");
		//LocalDate localDate = dateTime.toLocalDate();
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		
		Date desde = null;
		Date hasta = null; 
		
		if(recurso == null) {
			recurso = "A125958";
		}
		if (form.getDesde() == null) {
			desde = new Date();
			form.setDesde(formatter.format(desde));
		} else {
			desde = formatter.parse(form.getDesde());
		}
		if (form.getHasta() == null) {
			hasta = new Date();
			form.setHasta(formatter.format(hasta));
		} else {
			hasta = formatter.parse(form.getHasta());
		}
		
		logger.debug("Recurso: " + recurso);
		logger.debug("Desde  : " + desde);
		logger.debug("Hasta  : " + hasta);
		
		//List<HoraTrabajada> horas = horasRepository.findById("XA00309");
		
		List<HoraTrabajada> horas = horasService.findBy(recurso, desde, hasta);
		
		model.addAttribute("horas", horas);
		return "detalleHoras";
	}
	
	@RequestMapping(value="/cargar", method=RequestMethod.GET)
	public String cargarHoras(Model model) throws Exception {
		
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		
		model.addAttribute("horas", new Long(0));
		model.addAttribute("minutos", new Long(0));
		model.addAttribute("tipos", TipoHora.values());
		
		Date inicioMes = formatter.parse("01-05-2011");
		Date finMes = formatter.parse("31-05-2011");

		//List<Feriado> feriados = feriadoService.findAll(inicioMes, finMes);
		
		Map<Feriado, Boolean> feriados = feriadoService.getPeriodo(inicioMes, finMes);
		
		model.addAttribute("feriados", feriados);
		model.addAttribute("cargaHora", new HoraTrabajada());
		return "cargaHorasForm";
	}
	
	@RequestMapping(value="/guardar", method=RequestMethod.POST)
	public String processRegistration(
			@Valid HoraTrabajada hora,
			Model model,
			Errors errors) throws IllegalStateException, IOException {
		logger.debug("Grabando los datos...");
		logger.debug("hora.getDesde(): " + hora.getDesde().toString());
		
		hora.setRecurso(new Recurso("A125958"));
		
		
		if (errors.hasErrors()) {
			return "cargarHoras";
		}
		horasService.save(hora);
		return "redirect:/horas/cargar";
	}
}
