package ar.com.bbva.micro.cgm.dao;

import java.util.Date;
import java.util.List;

import ar.com.bbva.micro.cgm.model.HoraTrabajada;

public interface HorasRepository {
	
	List<HoraTrabajada> findById(String recurso);
	
	List<HoraTrabajada> findBy(String recurso, Date desde, Date hasta);
	
	List<HoraTrabajada> findHoras(long max, int count);

	HoraTrabajada findOne();
	
	void save(HoraTrabajada hora);
}
