package ar.com.bbva.micro.cgm.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import ar.com.bbva.micro.cgm.dao.HorasRepository;
import ar.com.bbva.micro.cgm.model.HoraTrabajada;

@Service
public class HorasService {
	@Autowired
	@Qualifier("jdbcHorasRepository")
	HorasRepository repository;
	
	public List<HoraTrabajada> findBy(String recurso, Date desde, Date hasta) {
		return new ArrayList<HoraTrabajada>(repository.findBy(recurso, desde, hasta));
	}
	
	public void save(HoraTrabajada hora) {
		repository.save(hora);
	}
}
