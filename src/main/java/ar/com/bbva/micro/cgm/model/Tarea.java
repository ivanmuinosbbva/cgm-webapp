package ar.com.bbva.micro.cgm.model;

public class Tarea {
	private String codigo;
	
	public Tarea() {}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Tarea(String codigo) {
		super();
		this.codigo = codigo;
	}
}
