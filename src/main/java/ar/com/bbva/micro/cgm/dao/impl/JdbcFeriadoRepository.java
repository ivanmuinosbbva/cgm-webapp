package ar.com.bbva.micro.cgm.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.stereotype.Repository;

import ar.com.bbva.micro.cgm.dao.FeriadoRepository;
import ar.com.bbva.micro.cgm.dao.mappers.FeriadoMapper;
import ar.com.bbva.micro.cgm.model.Feriado;

@Repository("jdbcFeriadoRepository")
public class JdbcFeriadoRepository implements FeriadoRepository {
	Logger logger = LoggerFactory.getLogger(JdbcFeriadoRepository.class);
	
	private JdbcOperations jdbcOperations;
	
	@Autowired
	public JdbcFeriadoRepository(JdbcOperations jdbcOperations) {
		this.jdbcOperations = jdbcOperations;
	}
	
	@Override
	public List<Feriado> findAll() {
		List<Feriado> feriados = new ArrayList<Feriado>();
		
		feriados = jdbcOperations.query("SELECT fecha FROM Feriado ORDER BY fecha", new FeriadoMapper());
		return feriados;
	}

	@Override
	public List<Feriado> findAll(Date desde, Date hasta) {
		List<Feriado> feriados = new ArrayList<Feriado>();
		
		Object[] args = {desde, hasta};
		
		feriados = jdbcOperations.query("SELECT fecha FROM Feriado WHERE fecha >= ? and fecha <= ? ORDER BY fecha", 
				args,
				new FeriadoMapper());
		return feriados;
	}

}
