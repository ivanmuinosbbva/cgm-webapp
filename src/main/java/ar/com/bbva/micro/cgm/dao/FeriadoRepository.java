package ar.com.bbva.micro.cgm.dao;

import java.util.Date;
import java.util.List;

import ar.com.bbva.micro.cgm.model.Feriado;

public interface FeriadoRepository {
	List<Feriado> findAll();
	
	List<Feriado> findAll(Date desde, Date hasta);
}
