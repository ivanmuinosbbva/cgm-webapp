package ar.com.bbva.micro.cgm.model;

public class Peticion {
	private long interno;
	
	public Peticion() {}

	public long getInterno() {
		return interno;
	}

	public void setInterno(long interno) {
		this.interno = interno;
	}

	public Peticion(long interno) {
		super();
		this.interno = interno;
	}
}
