package ar.com.bbva.micro.cgm.model;

import java.beans.PropertyEditorSupport;

import org.springframework.util.StringUtils;

public class PeticionEditor extends PropertyEditorSupport {
	@Override
    public String getAsText() {
        Peticion peticion = (Peticion) getValue();
         
        return peticion == null ? "" : String.valueOf(peticion.getInterno());
    }
	
	@Override
    public void setAsText(String text) throws IllegalArgumentException {
		if (StringUtils.isEmpty(text)) {
            setValue(null);
        } else {
        	Peticion peticion = new Peticion();
        	peticion.setInterno(Long.parseLong(text));
        	setValue(peticion);
        }
	}
}
