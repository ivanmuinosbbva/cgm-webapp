package ar.com.bbva.micro.cgm.dao.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import ar.com.bbva.micro.cgm.model.Feriado;

public class FeriadoMapper implements RowMapper<Feriado> {

	@Override
	public Feriado mapRow(ResultSet rs, int rowNum) throws SQLException {
		Feriado feriado = new Feriado();
		
		feriado.setFecha(rs.getDate("fecha"));
		return feriado;
	}
}
