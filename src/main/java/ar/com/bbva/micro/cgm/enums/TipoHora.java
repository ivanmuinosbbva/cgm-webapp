package ar.com.bbva.micro.cgm.enums;

public enum TipoHora {
	DESARROLLO("DESA", "Desarrollo"), REFINAMIENTO("REFI", "Refinamiento");
	
	private String codigo;
	private String descripcion;
	
	private TipoHora(String codigo, String descripcion) {
		this.codigo = codigo;
		this.descripcion = descripcion;
	}
	
	public String getCodigo() {
		return codigo;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	@Override
	public String toString() {
		return this.name();
	}
}
