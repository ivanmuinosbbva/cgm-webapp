package ar.com.bbva.micro.cgm.config;

import java.util.regex.Pattern;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.core.type.filter.RegexPatternTypeFilter;
import org.springframework.jdbc.core.JdbcTemplate;

import ar.com.bbva.micro.cgm.config.RootConfig.WebPackage;

@Configuration
@ComponentScan(basePackages = {"ar.com.bbva.micro.cgm"},
		excludeFilters = { @Filter(type=FilterType.CUSTOM, value = WebPackage.class)})
public class RootConfig {
	
	@Bean
	public BasicDataSource dataSource() {
		BasicDataSource ds = new BasicDataSource();

		ds.setDriverClassName("net.sourceforge.jtds.jdbc.Driver");
		ds.setUrl("jdbc:jtds:sybase://conrad:5000/GesPet");
		ds.setUsername("sa");
		ds.setPassword("pepetrueno");
		ds.setInitialSize(3);
		ds.setValidationQuery("select 1");
		return ds;
	}
	
	@Bean
	public JdbcTemplate jdbcTemplate(DataSource dataSource) {
		return new JdbcTemplate(dataSource);
	}
	
	public static class WebPackage extends RegexPatternTypeFilter {
		public WebPackage() {
			super(Pattern.compile("ar.com.bbva.micro.cgm\\\\.web"));
		}
	}
}
